use anyhow::Result;
use chrono::NaiveDate;
use cpp::cpp;
use qmetaobject::{QAbstractTableModel, QObject, QString, QObjectBox, qmetaobject_lazy_static, qt_base_class, qt_method};
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};
use qmetaobject::lazy_static;

use crate::lifetimed;

const NAME: &str = "quaddicted";
const EXT: &str = "json";
const DB: &str = "https://www.quaddicted.com/reviews/quaddicted_database.xml";
const BASE: &str = "https://www.quaddicted.com/filebase/";
const IMGS: &str = "https://www.quaddicted.com/reviews/screenshots/";

#[derive(Debug, Deserialize, Serialize)]
pub struct File {
    pub id: String,
    #[serde(rename = "type")]
    pub kind: u8,
    // Can be empty
    pub rating: String,
    pub author: String,
    pub title: String,
    pub md5sum: String,
	pub size: u32,
	#[serde(deserialize_with = "de_date")]
    pub date: NaiveDate,
    pub description: String,
	pub techinfo: Techinfo,

	#[serde(default)]
	pub installed: bool,

	#[serde(default)]
	pub cdate: Option<NaiveDate>,
}

pub fn de_date<'a, 'de, D: serde::Deserializer<'de>>(deserializer: D) -> Result<NaiveDate, D::Error> {
	struct DateVisitor;
	impl<'de> serde::de::Visitor<'de> for DateVisitor {
		type Value = NaiveDate;

		fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
			write!(formatter, "a date encoded in either dd.mm.yy format or rfc")
		}

		fn visit_str<E: serde::de::Error>(self, v: &str) -> Result<Self::Value, E> {
			use std::str::FromStr;
			match NaiveDate::from_str(v) {
				Ok(date) => Ok(date),
				// TODO shouldn't discard error info but serde errors are awkward
				Err(e) => NaiveDate::parse_from_str(v, "%d.%m.%y").map_err(serde::de::Error::custom),
			}
		}
	}

	deserializer.deserialize_str(DateVisitor)
}

impl File {
    async fn get<P: AsRef<Path>>(&mut self, dir: P) -> Result<()> {
		if self.installed {
			log::warn!("Reinstalling {}", self.title)
		}
        let response = reqwest::get(&[BASE, &self.id, ".zip"].concat()).await?;
        let mut archive = {
            let bytes = response.bytes().await?;
            let cursor = std::io::Cursor::new(bytes);
            zip::ZipArchive::new(cursor)?
		};
		
		if archive.is_empty() {
			log::warn!("Empty archive retrieved for: {}", self.title);
			return Ok(())
		}

		// Can't interate over file names and index by name... wtb alternative to this deep copy
        let filenames: Vec<String> = archive.file_names().map(|s| s.to_owned()).collect();
        for name in filenames {
			// Copy takes a mut reference to a file?
            let mut zipfile = archive.by_name(&name)?;
            let mut path = dir.as_ref().to_owned();
            path.push(zipfile.name());
            log::info!("File in archive: {} {} {}", name, zipfile.name(), zipfile.size());
            if zipfile.is_dir() {
                tokio::fs::create_dir_all(&path).await?;
            } else if zipfile.is_file() {
                let mut file = std::fs::File::create(&path)?;
                std::io::copy(&mut zipfile, &mut file)?;
            } else {
                unreachable!()
            }
		}
		self.installed = true;
        Ok(())
	}
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Techinfo {
    #[serde(default)]
    pub zipbasedir: String,
    #[serde(default)]
    pub commandline: String,
    #[serde(rename = "startmap", default)]
    pub startmaps: Vec<String>,
    #[serde(default)]
    pub requirements: Requirements,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct Requirements {
    #[serde(rename = "file")]
    pub files: Vec<Requirement>
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Requirement { pub id: String }

#[derive(Debug, Deserialize, Serialize)]
pub struct Files {
    #[serde(rename = "file")]
    pub files: Vec<File>,
}

#[derive(QObject)]
pub struct Backend<'a> {
    base: qt_base_class!(trait QAbstractTableModel),
    files: Vec<File>,
    settings: &'a crate::Settings,

    print: qt_method!(fn print(&self) {
        log::info!("Quaddicted print method invoked # files {}", self.files.len());
        for file in &self.files {
            log::info!("Quaddicted file: {} {} {} {}", file.title, file.author, file.date, file.size);
        }
	}),

	sort: qt_method!(fn sort(&mut self, col: i32) {
		if col < 0 || col > 3 {
			log::warn!("Invalid column number passed to sort: {}", col);
			return
		}
		self.about_to_change();
		self.files.sort_by(match col {
			0 => |a: &File, b: &File| {
				a.title.cmp(&b.title)
			},
			1 => |a: &File, b: &File| {
				a.author.cmp(&b.author)
			},
			2 => |a: &File, b: &File| {
				a.date.cmp(&b.date)
			},
			3 => |a: &File, b: &File| {
				a.rating.cmp(&b.rating)
			},
			_ => unreachable!(),
		});
		self.changed();
	}),

	reverse: qt_method!( fn reverse(&mut self) {
		self.about_to_change();
		self.files.reverse();
		self.changed();
	}),

	refresh: qt_method!(fn refresh(&mut self) {
		// Doing this sync can take a while but cannot spawn an async task b/c of self ref and
		// self having a settings ref. Should settings be a rc?
		if let Ok(mut files) = Backend::get_sync() {
			if self.files.len() != files.len() {
				self.about_to_change();
				self.files.clear();
				self.files.append(&mut files);
				self.changed();
			}
		}
	})
}

impl<'a> QAbstractTableModel for Backend<'a> {
    fn row_count(&self) -> i32 {
        self.files.len() as i32
	}

	fn column_count(&self) -> i32 {
		4
	}

    fn data(&self, index: qmetaobject::QModelIndex, _: i32) -> qmetaobject::QVariant {
		let file = &self.files[index.row() as usize];
		match index.column() {
			0 => QString::from(lifetimed(&file.title)).into(),
			1 => QString::from(lifetimed(&file.author)).into(),
			2 => qmetaobject::QDate::from(file.date.clone()).into(),
			3 => QString::from(lifetimed(&file.rating)).into(),
			_ => unreachable!(),
		}
    }
}

impl<'a> Drop for Backend<'a> {
    fn drop(&mut self) {
        if self.files.len() > 0 {
            let _ = self.save();
        }
    }
}

cpp! {{
	#include <QtCore/QAbstractListModel>
}}

impl<'a> Backend<'a> {
    pub async fn new(settings: &'a crate::Settings) -> Result<QObjectBox<Backend<'a>>> {
        let mut dest: PathBuf = settings.data_dir.0.clone();
        dest.push(&NAME);
        dest.set_extension(&EXT);

        let files = match std::fs::File::open(&dest) {
            Ok(file) => serde_json::from_reader(file)?,
            Err(e) => {
                log::info!("File open failed: {}", e);
                Self::get().await?
            }
        };

        Ok(QObjectBox::new(Self {base: Default::default(), files, settings, print: Default::default(), sort: Default::default(), reverse: Default::default(), refresh: Default::default()}))
    }

    async fn get() -> Result<Vec<File>> {
        let text = reqwest::get(DB).await?.text().await?;
        let files: Files = serde_xml_rs::de::from_str(&text)?;
        Ok(files.files)
	}

	fn get_sync() -> Result<Vec<File>> {
		let text: String = reqwest::blocking::get( DB)?.text()?;
        let files: Files = serde_xml_rs::de::from_str(&text)?;
        Ok(files.files)
	}
	
	fn about_to_change(&self) {
		let obj = self.get_cpp_object();
		cpp!(unsafe [obj as "QAbstractListModel *"] {
			emit obj->layoutAboutToBeChanged({}, QAbstractItemModel::VerticalSortHint);
		});
	}

	fn changed(&self) {
		let obj = self.get_cpp_object();
		cpp!(unsafe [obj as "QAbstractListModel *"] {
			emit obj->layoutChanged({}, QAbstractItemModel::VerticalSortHint);
		});
	}

	async fn refresher(&mut self) {
        if let Ok(mut files) = Backend::get().await {
            if self.files.len() != files.len() {
				self.about_to_change();
                self.files.clear();
				self.files.append(&mut files);
				self.changed();
            }
		}
	}

    pub fn save(&self) -> Result<()> {
        if !self.settings.data_dir.exists() {
            std::fs::create_dir_all(&self.settings.data_dir.0)?;
        }
        let mut dest = self.settings.data_dir.0.clone();
        dest.push(&NAME);
        dest.set_extension(&EXT);
        
        let file = std::fs::File::create(&dest)?;
        serde_json::to_writer(file,&self.files)?;
        Ok(())
	}
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_file_deser() {
        let text = r##"
<file id="100b4" type="2" rating="4">
	<author>Artistical, DelusionalBear, Digs, dumptruck_ds, ionous, JCR, Newhouse, Redfield, Roman, Shamblernaut, Shotro, Spipper, Spud, ww</author>
	<title>100 Brush Competition #4</title>
	<md5sum>a90a75a2903fd846edc3dc8a7a3e0418</md5sum>
	<size>46606</size>
	<date>10.06.18</date>
	<description><![CDATA[Results of the fourth 100b contest (=maps made using <100 brushes): 19 singleplayer levels by various authors, and a start hub to select each of them. Some of the maps come with additional content such as models, sounds, and skyboxes. The map sources are included.<br/><br/>
Final results:<br/><br/>
1. Deflan'Ka by ww<br/>
2. Ioian Shorelines by ionous<br/>
3. Immortal Reach by Spipper<br/>
4. The Di[g]stortion by Digs<br/>
5. Frostbitten by ionous<br/>
6. With Apologies to Insomniac by Spud<br/>
7. The Vale of Dream by Redfield<br/>
8. (tie) The Teleportation Complex by Newhouse<br/>
8. (tie) Beyond and Within by Artistical<br/>
8. (tie) Between Dimensions by JCR<br/>
11. (tie) Locus Genus by Artistical<br/>
11. (tie) Pyro's Barbecue by Newhouse<br/>
11. (tie) The Forgotten Station by Artistical<br/>
14. (tie) Cerebral Edema by Shotro<br/>
14. (tie) The Engineering Department by JCR<br/>
16. The Tutorial by dumptruck_ds<br/>
17. N-Titties by Shamblernaut<br/>
18. Unwarranted Phlebotomy by DelusionalBear<br/>
19. Ela-V8 by Roman
<br/><br/><b>Note:</b> These maps require <a href="quoth2pt2full_2.html">Quoth 2.2</a> and in parts a source port with increased limits.]]></description>
	<techinfo>
		<zipbasedir>100b4/</zipbasedir>
		<commandline>-quoth -game 100b4</commandline>
		<startmap>start</startmap>
		<startmap>100b4_artistical01</startmap>
		<startmap>100b4_artistical_02</startmap>
		<startmap>100b4_artistical_03</startmap>
		<startmap>100b4_delusionalbear</startmap>
		<startmap>100b4_digs</startmap>
		<startmap>100b4_dumptruck</startmap>
		<startmap>100b4_ionfrost</startmap>
		<startmap>100b4_ionio</startmap>
		<startmap>100b4_jcr</startmap>
		<startmap>100b4_jcr2</startmap>
		<startmap>100b4_newhouse</startmap>
		<startmap>100b4_newhouse2</startmap>
		<startmap>100b4_redfield</startmap>
		<startmap>100b4_shamblernaut</startmap>
		<startmap>100b4_shotro</startmap>
		<startmap>100b4_spipper</startmap>
		<startmap>100b4_spud</startmap>
		<startmap>100b4_ww</startmap>
		<requirements>
			<file id="quoth2pt2full_2" />
		</requirements>
	</techinfo>
</file>"##;
        let _: File = serde_xml_rs::from_str(text).unwrap();
    }

    #[test]
    fn test_files_deser() {
        let text = r##"
<?xml version="1.0" encoding="UTF-8"?>
<files>
<file id="huh_final" type="2" rating="2">
	<author>Anon/Ranger, ASO3000, David, DiamondBlack, DocDuodenum, Redead ITA, Sobersamson, Sockstah, Streaky, ZungryWare</author>
	<title>HUH: A /vr/ mapping project</title>
	<md5sum>296900a01a4cab805bd70a2f4e32ef27</md5sum>
	<size>38193</size>
	<date>23.09.19</date>
	<description><![CDATA[A set of 11 beginner levels in various themes. Features a start map and a custom music track. The map sources are included.<br/><br/><b>Note:</b> One of the maps (huh_tombandtower) requires a source port with increased limits.]]></description>
	<techinfo>
		<zipbasedir>/</zipbasedir>
		<commandline>-game huh</commandline>
		<startmap>start</startmap>
		<startmap>huh_655321</startmap>
		<startmap>huh_babby</startmap>
		<startmap>huh_blacast</startmap>
		<startmap>huh_hangar8</startmap>
		<startmap>huh_ribcage</startmap>
		<startmap>huh_runegar</startmap>
		<startmap>huh_sobersamson</startmap>
		<startmap>huh_technotomb</startmap>
		<startmap>huh_tombandtower</startmap>
		<startmap>huh_tower</startmap>
		<startmap>huh_waydown</startmap>
	</techinfo>
</file>
<file id="hulk256" type="1" rating="4">
	<author>ijed</author>
	<title>Data Download</title>
	<md5sum>f22fcd01a8c8d0ab1f4b19800f9b0f67</md5sum>
	<size>550</size>
	<date>29.06.08</date>
	<description><![CDATA[A map built with no more than 256 brushes. Your mission is to download some data, but there are a couple of enemies who try to prevent that. Fast and frantic non-stop action.
<br /><br /><b>Note:</b> This map requires <a href="http://www.quaddicted.com/reviews/quoth.html">Quoth</a>.]]></description>
	<techinfo>
		<zipbasedir>quoth/maps/</zipbasedir>
		<commandline>-hipnotic -game quoth</commandline>
		<requirements>
			<file id="quoth" />
		</requirements>
	</techinfo>
</file>
<file id="human1320" type="1" rating="">
	<author>human[rus]</author>
	<title>Human1320</title>
	<md5sum>642bedd0709393441d60be7049777ce4</md5sum>
	<size>1075</size>
	<date>09.03.20</date>
	<description><![CDATA[Medium to large spacious medieval level with long corridors. The map source is included.<br/><br/><b>Note:</b> This map requires TF or MTF coop. Since there are no weapons/supplies, it cannot be played without.]]></description>
	<techinfo>
		<zipbasedir>id1/maps/</zipbasedir>
	</techinfo>
</file>
<file id="hunter" type="1" rating="2">
	<author>Richard Dale Carlson</author>
	<title>Demon Hunter</title>
	<md5sum>9a499b40153edabe90fd390964658eda</md5sum>
	<size>432</size>
	<date>13.02.97</date>
	<description><![CDATA[About medium sized confusing Wizard map.]]></description>
	<techinfo>
		<zipbasedir>id1/maps/</zipbasedir>
	</techinfo>
</file>
<file id="hush" type="1" rating="">
	<author>iYago</author>
	<title>Hushed Walls</title>
	<md5sum>740cf4332e9f121f1f0fc8da34afc31e</md5sum>
	<size>1306</size>
	<date>15.05.20</date>
	<description><![CDATA[Small to medium-sized level in Gloom Keep style, with a dark brick dungeon beneath. The map source is included.]]></description>
	<techinfo>
		<zipbasedir>id1/maps/</zipbasedir>
	</techinfo>
</file>
<file id="hwjam" type="2" rating="5">
	<author>Ing-ing, JCR, Newhouse, nyoeieoyn, PinchySkree, Qmaster, xlarve</author>
	<title>Halloween Jam 2018</title>
	<md5sum>85a74d4c2369dd0e188d5bc370f63625</md5sum>
	<size>89307</size>
	<date>18.11.18</date>
	<description><![CDATA[Community map pack featuring six mostly Halloween/Horror-themed levels including haunted houses and graveyards. It also comes with a start map and lots of pumpkins, and runs on a slighlty stripped-down version of Arcane Dimensions that is included. The map sources are included as well.<br/><br/><b>Note:</b> These maps require a source port with increased limits.]]></description>
	<techinfo>
		<zipbasedir>hwjam/</zipbasedir>
		<commandline>-game hwjam</commandline>
		<startmap>start</startmap>
		<startmap>hwjam_ing</startmap>
		<startmap>hwjam_jcr</startmap>
		<startmap>hwjam_nyo</startmap>
		<startmap>hwjam_pinchy</startmap>
		<startmap>hwjam_qmaster</startmap>
		<startmap>hwjam_xlarve</startmap>
	</techinfo>
</file>
<file id="hwjam2" type="2" rating="5">
	<author>Artistical, Burnham, CaptainCam, Chedap, Chris Holden, Comrade Beep, Greenwood, Here, JCR, Kayne, MortalMaxx, NewHouse, Pinchy, Thulsa, Yoder</author>
	<title>Halloween Jam 2</title>
	<md5sum>e648d6d23e418b25c3738f46f4937416</md5sum>
	<size>215061</size>
	<date>31.10.19</date>
	<description><![CDATA[Second Halloween Jam community project featuring 14 levels of varying styles, and a start map. The levels run on the Arcane Dimensions mod (included). The map sources are included.
<br/><br/><b>Note:</b> These maps require a source port with increased limits.]]></description>
	<techinfo>
		<zipbasedir>hwjam2/</zipbasedir>
		<commandline>-game hwjam2</commandline>
		<startmap>start</startmap>
		<startmap>hwjam2_artistical</startmap>
		<startmap>hwjam2_burnham</startmap>
		<startmap>hwjam2_captaincam</startmap>
		<startmap>hwjam2_chedap</startmap>
		<startmap>hwjam2_chrisholden</startmap>
		<startmap>hwjam2_comradebeep</startmap>
		<startmap>hwjam2_greenwood</startmap>
		<startmap>hwjam2_here</startmap>
		<startmap>hwjam2_jcr</startmap>
		<startmap>hwjam2_kayne</startmap>
		<startmap>hwjam2_maxx</startmap>
		<startmap>hwjam2_pinchy</startmap>
		<startmap>hwjam2_thulsa</startmap>
		<startmap>hwjam2_yoder</startmap>
	</techinfo>
</file>
</files>
        "##;
        let _ : Files = serde_xml_rs::from_str(text).unwrap();
    }

    #[test]
    fn test_array() {
        let text = r##"
<?xml version="1.0" encoding="UTF-8"?>
<file id="huh_final" type="2" rating="2">
	<author>Anon/Ranger, ASO3000, David, DiamondBlack, DocDuodenum, Redead ITA, Sobersamson, Sockstah, Streaky, ZungryWare</author>
	<title>HUH: A /vr/ mapping project</title>
	<md5sum>296900a01a4cab805bd70a2f4e32ef27</md5sum>
	<size>38193</size>
	<date>23.09.19</date>
	<description><![CDATA[A set of 11 beginner levels in various themes. Features a start map and a custom music track. The map sources are included.<br/><br/><b>Note:</b> One of the maps (huh_tombandtower) requires a source port with increased limits.]]></description>
	<techinfo>
		<zipbasedir>/</zipbasedir>
		<commandline>-game huh</commandline>
		<startmap>start</startmap>
		<startmap>huh_655321</startmap>
		<startmap>huh_babby</startmap>
		<startmap>huh_blacast</startmap>
		<startmap>huh_hangar8</startmap>
		<startmap>huh_ribcage</startmap>
		<startmap>huh_runegar</startmap>
		<startmap>huh_sobersamson</startmap>
		<startmap>huh_technotomb</startmap>
		<startmap>huh_tombandtower</startmap>
		<startmap>huh_tower</startmap>
		<startmap>huh_waydown</startmap>
	</techinfo>
</file>
<file id="hulk256" type="1" rating="4">
	<author>ijed</author>
	<title>Data Download</title>
	<md5sum>f22fcd01a8c8d0ab1f4b19800f9b0f67</md5sum>
	<size>550</size>
	<date>29.06.08</date>
	<description><![CDATA[A map built with no more than 256 brushes. Your mission is to download some data, but there are a couple of enemies who try to prevent that. Fast and frantic non-stop action.
<br /><br /><b>Note:</b> This map requires <a href="http://www.quaddicted.com/reviews/quoth.html">Quoth</a>.]]></description>
	<techinfo>
		<zipbasedir>quoth/maps/</zipbasedir>
		<commandline>-hipnotic -game quoth</commandline>
		<requirements>
			<file id="quoth" />
		</requirements>
	</techinfo>
</file>
<file id="human1320" type="1" rating="">
	<author>human[rus]</author>
	<title>Human1320</title>
	<md5sum>642bedd0709393441d60be7049777ce4</md5sum>
	<size>1075</size>
	<date>09.03.20</date>
	<description><![CDATA[Medium to large spacious medieval level with long corridors. The map source is included.<br/><br/><b>Note:</b> This map requires TF or MTF coop. Since there are no weapons/supplies, it cannot be played without.]]></description>
	<techinfo>
		<zipbasedir>id1/maps/</zipbasedir>
	</techinfo>
</file>
<file id="hunter" type="1" rating="2">
	<author>Richard Dale Carlson</author>
	<title>Demon Hunter</title>
	<md5sum>9a499b40153edabe90fd390964658eda</md5sum>
	<size>432</size>
	<date>13.02.97</date>
	<description><![CDATA[About medium sized confusing Wizard map.]]></description>
	<techinfo>
		<zipbasedir>id1/maps/</zipbasedir>
	</techinfo>
</file>
<file id="hush" type="1" rating="">
	<author>iYago</author>
	<title>Hushed Walls</title>
	<md5sum>740cf4332e9f121f1f0fc8da34afc31e</md5sum>
	<size>1306</size>
	<date>15.05.20</date>
	<description><![CDATA[Small to medium-sized level in Gloom Keep style, with a dark brick dungeon beneath. The map source is included.]]></description>
	<techinfo>
		<zipbasedir>id1/maps/</zipbasedir>
	</techinfo>
</file>
<file id="hwjam" type="2" rating="5">
	<author>Ing-ing, JCR, Newhouse, nyoeieoyn, PinchySkree, Qmaster, xlarve</author>
	<title>Halloween Jam 2018</title>
	<md5sum>85a74d4c2369dd0e188d5bc370f63625</md5sum>
	<size>89307</size>
	<date>18.11.18</date>
	<description><![CDATA[Community map pack featuring six mostly Halloween/Horror-themed levels including haunted houses and graveyards. It also comes with a start map and lots of pumpkins, and runs on a slighlty stripped-down version of Arcane Dimensions that is included. The map sources are included as well.<br/><br/><b>Note:</b> These maps require a source port with increased limits.]]></description>
	<techinfo>
		<zipbasedir>hwjam/</zipbasedir>
		<commandline>-game hwjam</commandline>
		<startmap>start</startmap>
		<startmap>hwjam_ing</startmap>
		<startmap>hwjam_jcr</startmap>
		<startmap>hwjam_nyo</startmap>
		<startmap>hwjam_pinchy</startmap>
		<startmap>hwjam_qmaster</startmap>
		<startmap>hwjam_xlarve</startmap>
	</techinfo>
</file>
<file id="hwjam2" type="2" rating="5">
	<author>Artistical, Burnham, CaptainCam, Chedap, Chris Holden, Comrade Beep, Greenwood, Here, JCR, Kayne, MortalMaxx, NewHouse, Pinchy, Thulsa, Yoder</author>
	<title>Halloween Jam 2</title>
	<md5sum>e648d6d23e418b25c3738f46f4937416</md5sum>
	<size>215061</size>
	<date>31.10.19</date>
	<description><![CDATA[Second Halloween Jam community project featuring 14 levels of varying styles, and a start map. The levels run on the Arcane Dimensions mod (included). The map sources are included.
<br/><br/><b>Note:</b> These maps require a source port with increased limits.]]></description>
	<techinfo>
		<zipbasedir>hwjam2/</zipbasedir>
		<commandline>-game hwjam2</commandline>
		<startmap>start</startmap>
		<startmap>hwjam2_artistical</startmap>
		<startmap>hwjam2_burnham</startmap>
		<startmap>hwjam2_captaincam</startmap>
		<startmap>hwjam2_chedap</startmap>
		<startmap>hwjam2_chrisholden</startmap>
		<startmap>hwjam2_comradebeep</startmap>
		<startmap>hwjam2_greenwood</startmap>
		<startmap>hwjam2_here</startmap>
		<startmap>hwjam2_jcr</startmap>
		<startmap>hwjam2_kayne</startmap>
		<startmap>hwjam2_maxx</startmap>
		<startmap>hwjam2_pinchy</startmap>
		<startmap>hwjam2_thulsa</startmap>
		<startmap>hwjam2_yoder</startmap>
	</techinfo>
</file>"##;
    let files : Vec<File> = serde_xml_rs::from_str(text).unwrap();
    assert!(files.len() > 0);
    }
}