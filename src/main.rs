use anyhow::Result;
//use chrono::{Datelike, Timelike};
use derive_more::{Deref, DerefMut};
use http::uri::Uri;
use is_executable::IsExecutable;
use qmetaobject::{lazy_static, /*QDate, QDateTime,*/ QObject, QObjectBox, QString, qmetaobject_lazy_static, qt_base_class, qt_method, qt_property, qt_signal};
use serde::{Deserialize, Serialize};
use std::path::PathBuf;

mod idgames;
mod quaddicted;

const APP_NAME: &str = "infuser";
const CFG_EXT: &str = "toml";

const DOOM_NAMES: &[&str] = &["gzdoom", "chocolate-doom", "crispy-doom", "zandronum", "zdoom", "doomsday", "doom"];
const QUAKE_NAMES: &[&str] = &["quakespasm", "vkquake", "fteqw", "darkplaces", "quake"];

lazy_static::lazy_static! {
    static ref CFG_PATH: PathBuf = {
        let mut config_path = dirs::config_dir().expect("Config dir not available on platform");
        config_path.push(APP_NAME);
        config_path.set_extension(CFG_EXT);
        config_path
    };
}

qmetaobject::qrc!(qrc, "" { "prelude.qml", "main.qml", "FileRow.qml" });

fn lifetimed<'a>(s: &'a str) -> &'a str {
    s
}

#[derive(Clone, Debug, Default, Deref, DerefMut, Deserialize, Serialize)]
pub struct Loc(pub PathBuf);

impl Loc {
    fn to_qstring(&self) -> QString {
        QString::from(&*self.to_string_lossy())
    }
    fn from_qstring(qstr: &QString) -> Self {
        let str: String = qstr.to_owned().into();
        Loc(PathBuf::from(str))
    }
}

impl qmetaobject::QMetaType for Loc {
    const CONVERSION_TO_STRING: Option<fn(&Self) -> QString> = Some(Loc::to_qstring);
    const CONVERSION_FROM_STRING: Option<fn(&QString) -> Self> = Some(Loc::from_qstring);
}

#[derive(Deref, DerefMut, Default, Clone, serde::Serialize, serde::Deserialize)]
struct Link(#[serde(with = "http_serde::uri")] Uri);

impl Link {
    fn to_qstring(&self) -> QString {
        QString::from(self.to_string())
    }
    fn from_qstring(qstr: &QString) -> Self {
        let str: String = qstr.to_owned().into();
        Link(str.parse::<Uri>().unwrap_or(Uri::default()))
    }
}
impl qmetaobject::QMetaType for Link {
    const CONVERSION_TO_STRING: Option<fn(&Self) -> QString> = Some(Link::to_qstring);
    const CONVERSION_FROM_STRING: Option<fn(&QString) -> Self> = Some(Link::from_qstring);
}

/*
Can't manually impl default to default all the data fields because
QObject prevents moves. Somehow default derive works though? Its
putting it in a refcell somewhere but how is default() -> Settings
working?
*/
#[allow(non_snake_case)]
#[derive(QObject, serde::Deserialize, serde::Serialize)]
pub struct Settings {
    #[serde(skip)]
    base: qt_base_class!(trait QObject),
    isodate: qt_property!(bool; NOTIFY isoDateChanged),
    data_dir: qt_property!(Loc; NOTIFY dataDirChanged),
    doom_bin: qt_property!(Loc; NOTIFY doomBinChanged),
    doom_dir: qt_property!(Loc; NOTIFY doomDirChanged),
    idgames_mirror: qt_property!(Link; NOTIFY idGamesMirrorChanged),
    quake_bin: qt_property!(Loc; NOTIFY quakeBinChanged),
    quake_dir: qt_property!(Loc; NOTIFY quakeDirChanged),

    #[serde(skip)]
    isoDateChanged: qt_signal!(),
    #[serde(skip)]
    dataDirChanged: qt_signal!(),
    #[serde(skip)]
    doomBinChanged: qt_signal!(),
    #[serde(skip)]
    doomDirChanged: qt_signal!(),
    #[serde(skip)]
    idGamesMirrorChanged: qt_signal!(),
    #[serde(skip)]
    quakeBinChanged: qt_signal!(),
    #[serde(skip)]
    quakeDirChanged: qt_signal!(),

    #[serde(skip)]
    save: qt_method!(fn save(&self) {
        if let Ok(content) = toml::ser::to_vec(&self) {
            let _ = std::fs::write(CFG_PATH.as_path(), content);
        }
    }),
    #[serde(skip)]
    validate_bin: qt_method!(fn validate_bin(&self, qstr: QString) -> bool {
        let str: String = qstr.into();
        let path = PathBuf::from(str.strip_prefix("file://").unwrap_or(&str));
        path.is_executable()
    }),
    #[serde(skip)]
    validate_dir: qt_method!(fn validate_dir(&self, qstr: QString) -> bool {
        let str: String = qstr.into();
        let path = PathBuf::from(str);
        log::info!("{}", path.to_string_lossy());
        path.is_dir()
    }),
    #[serde(skip)]
    validate_url: qt_method!(fn validate_url(&self, qstr: QString) -> bool {
        let str: String = qstr.into();
        str.parse::<Uri>().is_ok()
    }),
}

impl Settings {
    fn new() -> Settings {
        Settings::load().unwrap_or(Settings::default())
    }

    fn load() -> Result<Settings> {
        let content = std::fs::read_to_string(CFG_PATH.as_path())?;
        Ok(toml::from_str(&content)?)
    }
}

impl Default for Settings {
    fn default() -> Settings {
        let mut data_dir = Loc(dirs::data_dir().unwrap_or(PathBuf::new()));
        let mut doom_dir = data_dir.clone();
        let mut quake_dir = data_dir.clone();
        (*data_dir).push("infuser");
        (*doom_dir).push("doom");
        (*quake_dir).push("quake");

        let mut doom_bin = PathBuf::new();

        for name in DOOM_NAMES {
            if let Ok(path) = which::which(name) {
                doom_bin = path;
                break;
            }
        }

        let mut quake_bin = PathBuf::new();

        for name in QUAKE_NAMES {
            if let Ok(path) = which::which(name) {
                quake_bin = path;
                break;
            }
        }

        Settings {
            isodate: false,
            data_dir,
            doom_bin: Loc(doom_bin),
            doom_dir,
            idgames_mirror: Link(Uri::from_static("https://www.quaddicted.com/files/idgames/")),
            quake_bin: Loc(quake_bin),
            quake_dir,
            base: Default::default(),
            isoDateChanged: Default::default(),
            dataDirChanged: Default::default(), 
            doomBinChanged: Default::default(),
            doomDirChanged: Default::default(),
            idGamesMirrorChanged: Default::default(),
            quakeBinChanged: Default::default(),
            quakeDirChanged: Default::default(),
            save: Default::default(),
            validate_bin: Default::default(),
            validate_dir: Default::default(),
            validate_url: Default::default(),
        }
    }
}

/*
#[derive(QObject)]
struct Quake {
    base: qt_base_class!(trait QAbstractListModel),
    files: Vec<quaddicted::File>,

}

#[derive(QObject)]
struct Doom {
    base: qt_base_class!(trait QAbstractListModel),
    
}

#[derive(QObject)]
struct QuakeMod {
    base: qt_base_class!(trait QObject),
}

#[derive(QObject)]
struct DoomMod {
    base: qt_base_class!(trait QObject),
}
push 

fn chrono_to_qdate(chrono: &chrono::NaiveDate) -> QDate {
    QDate::from_y_m_d(chrono.year(), chrono.month() as i32, chrono.day() as i32)
}

fn chrono_to_qdatetime(chrono: &chrono::DateTime<chrono::Local>) -> QDateTime {
    let date = chrono.date();
    let time = chrono.time();
    let qdate = QDate::from_y_m_d(date.year(), date.month() as i32, date.day() as i32);
    let qtime = qmetaobject::QTime::from_h_m_s_ms(time.hour() as i32, time.minute() as i32, Some(time.second() as i32), None);
    QDateTime::from_date_time_local_timezone(qdate, qtime)

}
*/
/*
#[derive(Debug, Deserialize)]
struct Foo {
    pub id: u8,
    pub author: String,
    pub size: u16
}

#[derive(Debug, Deserialize)]
struct Foos {
    #[serde(rename = "foo")]
    pub foos: Vec<Foo>
}*/

#[tokio::main]
async fn main() -> Result<()> {
    /*
    let raw = r##"
        <?xml version="1.0" encoding="UTF-8"?>
        <foos>
            <foo id="3">
                <author>Bean</author>
                <size>513</size>
            </foo>
            <foo id="15">
                <author>Cal</author>
                <size>1231</size>
            </foo>
            <foo id="4">
                <author>Tiff</author>
                <size>2155</size>
            </foo>
            <foo id="0">
                <author>Sprocket</author>
                <size>12</size>
            </foo>
        </foos>
    "##;

    let bar: Foos = serde_xml_rs::from_str(&raw)?;

    for foo in &bar.foos {
        println!("Foo name: {} {} {}", foo.id, foo.author, foo.size);
    }
    */

    simplelog::SimpleLogger::init(simplelog::LevelFilter::Trace, simplelog::Config::default())?;
    qmetaobject::log::init_qt_to_rust();
    qrc();
    let settings = QObjectBox::new(Settings::new());
    let idgames = idgames::Backend::new(settings.pinned().borrow().data_dir.0.clone());
    let pinned = settings.pinned();
    let quaddicted = quaddicted::Backend::new(pinned.borrow()).await?;

    let mut engine = qmetaobject::QmlEngine::new();
    engine.set_object_property("_settings".into(), pinned);
    engine.set_object_property("_idgames".into(), idgames.pinned());
    engine.set_object_property("_quaddicted".into(), quaddicted.pinned());
    engine.load_file("qrc:/prelude.qml".into());
    engine.load_file("qrc:/main.qml".into());
    engine.exec();
    Ok(())
}
