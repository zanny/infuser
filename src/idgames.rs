use anyhow::Result;
use chrono::{DateTime, TimeZone, Utc};
use serde::{Deserialize, Serialize};
use std::{collections::{BTreeSet, HashMap}, path:: {Path, PathBuf}};
use qmetaobject::{lazy_static, QAbstractListModel, QVariant, QObject, QString, QByteArray, QObjectBox, qt_base_class, qmetaobject_lazy_static};

use crate::lifetimed;

const NAME: &str = "idgames";
const EXT: &str = "json";
const API: &str = "https://www.doomworld.com/idgames/api/api.php?out=json";

#[derive(Clone, Default, Debug, qmetaobject::QGadget, Deserialize, Serialize)]
struct OptStr(Option<String>);

#[derive(Debug, Default, Deserialize, Serialize)]
struct File {
    pub id: u64,
    pub title: String,
    pub dir: String,
    pub filename: String,
    pub size: u32,
    pub age: u64,
    pub date: String,
    pub author: String,
    pub email: OptStr,
    pub description: String,
    pub rating: f32,
    pub votes: u16,
    #[serde(with = "http_serde::uri")]
    pub url: http::Uri,
}

impl File {
    async fn get<P: AsRef<Path>>(&self, dir: P, mut mirror: reqwest::Url) -> Result<String> {
        match mirror.path_segments_mut() {
            Ok(mut segments) => { segments.push(&self.dir).push(&self.filename); },
            Err(()) => return Err(anyhow::anyhow!("Can't get mutable path segments")),
        }
        let response = reqwest::get(mirror).await?;
        let mut archive = {
            let bytes = response.bytes().await?; 
            let cursor = std::io::Cursor::new(bytes);
            zip::ZipArchive::new(cursor)?
        };
        let mut wad = String::new();
        let names: Vec<String> = archive.file_names().map(|s| s.to_string()).collect();
        for name in &names {
            let mut zipfile = archive.by_name(name)?;
            let mut dest = dir.as_ref().to_owned();
            let lc = zipfile.name().to_lowercase();
            dest.push(zipfile.name().to_lowercase());
            ::log::info!("File in archive: {} {} {}", name, zipfile.name(), zipfile.size());
            if zipfile.is_dir() {
                tokio::fs::create_dir_all(&dest).await?;
            } else if zipfile.is_file() {
                if let Some(ext) = dest.extension() {
                    if ext == "wad" {
                        wad = lc;
                        log::info!("Found wad in archive: {} {}", zipfile.name(), self.filename);
                    }
                }
                let mut file = std::fs::File::create(&dest)?;
                // FIXME THIS IS BAD IN ASYNC
                std::io::copy(&mut zipfile, &mut file)?;
            } else {
                unreachable!()
            }
        }
        Ok(wad)
    }
}

#[derive(Deserialize, Serialize)]
pub struct Directory {
    id: u64,
    name: String,
    parent: u64,
    dirs: Vec<u64>,
    files: Vec<u64>,
    synced: DateTime<Utc>,
}

impl Default for Directory {
    fn default() -> Self {
        Self {
            id: Default::default(),
            name: Default::default(),
            parent: Default::default(),
            dirs: Default::default(),
            files: Default::default(),
            synced: Utc.timestamp(0, 0),
        }
    }
}

#[derive(Default, Deserialize, QObject, Serialize)]
pub struct Backend {
    #[serde(skip)]
    base: qt_base_class!(trait QAbstractListModel),
    dirs: HashMap<u64, Directory>,
    files: HashMap<u64, File>,
    #[serde(skip)]
    current: qmetaobject::qt_property!(u64; NOTIFY current_changed),
    #[serde(skip)]
    path: PathBuf,

    #[serde(skip)]
    current_changed: qmetaobject::qt_signal!(),

    #[serde(skip)]
    print: qmetaobject::qt_method!(fn print(&self) {
        log::info!("idGames print method invoked # dirs {} # files {}", self.dirs.len(), self.files.len());
        for dir in &self.dirs {
            log::info!("Directory {} {} {}", dir.1.name, dir.1.id, dir.1.synced);
        }

        for file in &self.files {
            log::info!("File {} {} {} {}", file.1.title, file.1.author, file.1.date, file.1.size);
        }
    })
}

impl Drop for Backend {
    fn drop(&mut self) {
        if self.dirs.len() > 0 {
            let _ = self.save();
        }
    }
}

impl QAbstractListModel for Backend {
    fn row_count(&self) -> i32 {
        match &self.dirs.get(&self.current) {
            Some(dir) => (dir.dirs.len() + dir.files.len()) as i32,
            None => 0,
        }
    }

    fn data(&self, index: qmetaobject::QModelIndex, role: i32) -> QVariant {
        if index.is_valid() && role == 0 {
            let current = &self.dirs[&self.current];
            if index.row() < current.dirs.len() as i32 {
                let target = &self.dirs[&current.dirs[index.row() as usize]];
                QString::from(lifetimed(&target.name)).into()
            } else {
                let file = &self.files[&current.files[index.row() as usize - self.dirs.len()]];
                match index.column() {
                    0 => QString::from(lifetimed(&file.title)).into(),
                    1 => QString::from(lifetimed(&file.author)).into(),
                    2 => QString::from(lifetimed(&file.date)).into(),
                    3 => file.size.into(),
                    4 => file.rating.into(),
                    5 => QString::from(lifetimed(&file.description)).into(),
                    _ => QVariant::default()
                }
            }
        } else {
            log::warn!("Data call with invalid index {} or non-display role {}", index.is_valid(), role);
            QVariant::default()
        }
    }

    fn role_names(&self) -> std::collections::HashMap<i32, QByteArray> {
        let mut map = std::collections::HashMap::new();
        map.insert(0, QByteArray::from("display"));
        map
    }
}

impl Backend {
    pub fn new(mut dir: PathBuf) -> QObjectBox<Backend> {
        dir.push(&NAME);
        dir.set_extension(&EXT);
        let obj: QObjectBox<Backend> = if let Ok(file) = std::fs::File::open(&dir) {
            log::info!("Trying to deserialize {}", dir.to_string_lossy());
            if let Ok(backend) = serde_json::from_reader(file) {
                 QObjectBox::new(backend)
            } else {
                QObjectBox::new(Default::default())
            }
        } else {
            QObjectBox::new(Default::default())
        };
        obj.pinned().borrow_mut().path = dir;
        obj
    }

    pub async fn sync(&mut self) -> Result<()> {
        let mut remove = Vec::new();
        let mut add = Vec::new();
        {
            let json: serde_json::Value = reqwest::get(&[API, 
                                                              "&action=getcontents&id=", 
                                                              &self.current.to_string()]
                                                              .concat()).await?.json().await?;
            log::debug!("Response json for dir {}: {}", self.current, json);
            if self.current == 0 && self.dirs.is_empty() {
                self.dirs.insert(0, Directory::default());
            }
            let mut current: &mut Directory = self.dirs.get_mut(&self.current).ok_or(anyhow::anyhow!("Missing directory for current index? {}", self.current))?;
            let mut common = BTreeSet::new();
            if let Some(dirs) = json["content"]["dir"].as_array() {
                for dir in dirs {
                    let id = dir["id"].as_u64();
                    let name = dir["name"].as_str();
                    if let (Some(id), Some(name)) = (id, name) {
                        if !current.files.contains(&(id)) {
                            current.files.push(id);
                            add.push((id, name.to_owned()));
                        } else {
                            common.insert(id);
                        }
                    }
                }
            }
            let mut updated = !add.is_empty();
            for id in &current.dirs {
                if !common.contains(id) {
                    updated = true;
                    remove.push(*id);
                }
            }
            
            let file = &json["content"]["file"];
            common.clear();
            let mut files: Vec<File> = serde_json::from_value(file.clone()).unwrap_or(Default::default());
            files.retain(|file| {
                if current.files.contains(&file.id) {
                    common.insert(file.id);
                    false
                } else {
                    true
                }
            });

            for id in &current.files {
                if !common.contains(id) {
                    updated = true;
                    self.files.remove(&id);
                }
            }
            
            if updated {
                current.synced = Utc::now();
            }
        }

        for (id, name) in add {
            self.dirs.insert(id, Directory {
                id, 
                name, 
                parent: self.current, 
                synced: Utc.timestamp(0, 0),
                ..Default::default()
            });
        }
        Ok(())
    }

    pub fn save(&self) -> Result<()> {
        let file = std::fs::File::create(&self.path)?;
        serde_json::to_writer(file, &self).map_err(|e| e.into())

    }
}

#[cfg(test)]
mod tests {

        #[test]
        fn get() {

        }

        #[test]
        fn save() {

        }

        #[test]
        fn load() {

        }
}