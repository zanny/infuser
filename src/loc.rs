use qmetaobject::QString;
use std::path::PathBuf;

#[derive(derive_more::Deref, derive_more::DerefMut, Debug, Default, Clone, serde::Serialize, serde::Deserialize)]
pub struct Loc(pub PathBuf);

impl Loc {
    fn to_qstring(&self) -> QString {
        QString::from(&*self.to_string_lossy())
    }
    fn from_qstring(qstr: &QString) -> Self {
        let str: String = qstr.to_owned().into();
        Loc(PathBuf::from(str))
    }
}

impl qmetaobject::QMetaType for Loc {
    const CONVERSION_TO_STRING: Option<fn(&Self) -> QString> = Some(Loc::to_qstring);
    const CONVERSION_FROM_STRING: Option<fn(&QString) -> Self> = Some(Loc::from_qstring);
}
