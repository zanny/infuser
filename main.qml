import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.15
import Qt.labs.qmlmodels 1.0
import Qt.labs.settings 1.0
import org.kde.kirigami 2.14 as Kg

Kg.ApplicationWindow {
    id: window
    minimumHeight: options.height
    minimumWidth: options.width
    property bool wide: width >= options.width + 400

    Settings {
        id: state
        property bool quakeMode: !_settings.doom_bin && _settings.quake_bin
        property int last
    }
    Column {
        id: controls
        height: window.height
        width: window.wide ? window.width - options.width - 60 : window.width

        ToolBar {
            width: parent.width
            Row {
                ToolButton {
                    checked: !state.quakeMode
                    text: qsTr("Doom")
                    onClicked: state.quakeMode = !state.quakeMode
                }
                ToolButton {
                    checked: state.quakeMode
                    text: qsTr("Quake")
                    onClicked: state.quakeMode = !state.quakeMode
                }
                ToolButton {
                    icon.name: "view-refresh"
                }
                ToolButton {
                    icon.name: "edit-download"
                }
                ToolButton {
                    icon.name: "edit-delete"
                }
                ToolButton {
                    icon.name: "view-hidden"
                }
                ToolButton {
                    icon.name: "view-visible"
                }
                ToolButton {
                    icon.name: "dialog-cancel"
                }
                ToolButton {
                    id: configure
                    icon.name: "configure"
                    checked: !content.visible && window.wide
                    onClicked: {
                        content.visible = false
                        if (!window.wide) {
                            checked = true
                        }
                    }
                }
            }
        }
        ToolBar {
            width: parent.width
            RowLayout {
                Repeater {
                    model: ["Title", "Author", "Date", "Rating"]
                    ToolButton {
                        Layout.preferredWidth: table.columnWidthProvider(index)
                        autoExclusive: true
                        text: modelData
                        onClicked: {
                            if(checked) {
                                _quaddicted.reverse()
                            }
                            else {
                                checked = true
                                _quaddicted.sort(index)
                            }
                        }
                    }
                }
            }
        }
        Button {
            id: updir
            width: parent.width
            visible: !state.quakeMode && _idgames.current != 0
            text: "Up"
            icon.name: "go-up"
        }
        TableView {
            id: table
            width: parent.width
            height: parent.height - y
            contentWidth: width
            onWidthChanged:  forceLayout()
            model: state.quakeMode ? _quaddicted : _idgames
            ScrollBar.vertical: ScrollBar { 
            }

            columnWidthProvider: function(column) {
                if(column === 0) {
                    return width * .35
                } else if(column === 1) {
                    return width * (_settings.isodate ? .35 : .25)
                } else if(column === 2) {
                    return width * (_settings.isodate ? .15 : .25)
                } else {
                    return width * .15
                }
            }

            delegate: Label {
                background: Rectangle {
                    color: row % 2 ? "transparent" : Kg.Theme.alternateBackgroundColor
                }
                clip: true
                elide: Text.ElideRight
                text: {
                    if(column === 2) {
                        if(_settings.isodate) {
                            display.toISOString().split('T')[0]
                        } else {
                            display.toLocaleDateString().replace(/^[A-Za-z]{6,9}, /, '')
                        }
                    } else if(column === 3) {
                        let num = parseInt(display)
                        "★".repeat(num) + "☆".repeat(5 - num)
                    } else {
                        display
                    }
                }

                TapHandler {
                    onTapped: content.populate(_quaddicted.data(row))
                    onDoubleTapped: _quaddicted.start(row)
                }
            }
        }
    }

    Item {
        id: content
        visible: false
        anchors.left: parent.wide ? controls.right : parent.left
        z: 1

        function populate(data) {
            
        }

    }

    Kg.FormLayout {
        id: options

        anchors.right: parent.right
        anchors.leftMargin: 20
        anchors.bottomMargin: anchors.leftMargin
        anchors.rightMargin: anchors.leftMargin
        visible: configure.checked
        z: 1

        Label {
            Kg.FormData.label: qsTr("Settings")
            Kg.FormData.isSection: true
        }
        Row {
            Kg.FormData.label: qsTr("Date")
            Button {
                checked: !_settings.isodate
                text: qsTr("Pretty")
                onClicked: _settings.isodate = false
            }
            Button {
                checked: _settings.isodate
                text: qsTr("ISO")
                onClicked: _settings.isodate = true
            }
        }
        Kg.Separator {
            Kg.FormData.label: qsTr("Doom")
            Kg.FormData.isSection: true
        }
        FileRow {
            Kg.FormData.label: qsTr("Executable")
            placeholder: qsTr("Path to Doom binary")
            title: qsTr("Select Doom executable to use")
            target: _settings.doom_bin
            onTargetChanged: _settings.doom_bin = target
        }
        FileRow {
            Kg.FormData.label: qsTr("Data Directory")
            placeholder: qsTr("Where doom.wad etc. are")
            title: qsTr("Select Doom data directory containing doom.wad, etc.")
            dir: true
            target: _settings.doom_dir
            onTargetChanged: _settings.doom_dir = target
        }
        TextField {
            Layout.fillWidth: true
            Kg.FormData.label: qsTr("idGames Mirror URL")
            placeholderText: qsTr("Should be an http file directory")
            text: _settings.idgames_mirror
            inputMethodHints: Qt.ImhUrlCharactersOnly 
            validator: RegularExpressionValidator { regularExpression: /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?\// }
        }
        Kg.Separator {
            Kg.FormData.label: qsTr("Quake")
            Kg.FormData.isSection: true
        }
        FileRow {
            Kg.FormData.label: qsTr("Executable")
            placeholder: qsTr("Path to Quake binary")
            title: qsTr("Select Quake executable to use")
            target: _settings.quake_bin
            onTargetChanged: _settings.quake_bin = target
        }
        FileRow {
            Kg.FormData.label: qsTr("Data Directory")
            placeholder: qsTr("Should contain id1, hipnotic, etc.")
            title: qsTr("Select Quake data directory containing the id1, hipnotic, etc. directories")
            dir: true
            target: _settings.quake_dir
            onTargetChanged: _settings.quake_dir = target
        }
        Kg.Separator {
            Kg.FormData.isSection: true
        }
        Button {
            id: save_conf
            Layout.fillWidth: true
            enabled: false
            text: qsTr("Save Changes")
            onClicked: {
                _settings.save()
                enabled = false
            }

            Connections {
                target: _settings
                function onIsoDateChanged() {
                    save_conf.enabled = true
                }
                function onDataDirChanged() {
                    save_conf.enabled = true
                }
                function onDoomBinChanged() {
                    save_conf.enabled = true
                }
                function onDoomDirChanged() {
                    save_conf.enabled = true
                }
                function onIdGamesMirrorChanged() {
                    save_conf.enabled = true
                }
                function onQuakeBinChanged() {
                    save_conf.enabled = true
                }
                function onQuakeDirChanged() {
                    save_conf.enabled = true
                }
            }
        }
    }
}
