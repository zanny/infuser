# Infuser

A QMetaObject Rust based mod manager for Doom & Quake using the idgames API and Quaddicted.

# Installation

You need Qt development headers for qmetaobject and the resultant binary needs Gui, Core, Quick, and Qml available in PATH. And Cargo. Then its just `cargo install`.

# License

[GPLv3](COPYING.md)
