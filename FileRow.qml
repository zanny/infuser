import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.15

RowLayout {
    id: root
    Layout.fillWidth: true

    property alias placeholder: textField.placeholderText
    property alias title: dialog.title
    property alias dir: dialog.selectFolder
    property string target

    TextField {
        Layout.fillWidth: true
        id: textField
        text: target
        onEditingFinished: {
            if(text != target && _settings.validate_bin(text)) {
                target = text
            }
        }
    }
    Button {
        Layout.fillHeight: true
        Layout.preferredWidth: height
        icon.name: "folder"
        onClicked: dialog.open()

        FileDialog {
            id: dialog
            onAccepted: {
                let local = fileUrl.toString().replace("file://", "")
                if((selectFolder && _settings.validate_dir(local)) || _settings.validate_bin(local)) {
                    target = local
                    textField.text = local
                }
            }
        }
    }
}