import QtQml 2.14

QtObject {
    Component.onCompleted: {
        Qt.application.version = "0.1"
        Qt.application.name = "infuser"
        Qt.application.organization = "infuser"
        Qt.application.domain = "foo.bar"
    }
}
